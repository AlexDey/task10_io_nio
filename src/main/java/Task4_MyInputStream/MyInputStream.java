package Task4_MyInputStream;

import java.io.*;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 8 Dec 2018
 * An implementation of InputStream with capability of push read data
 * back to the stream.
 */
public class MyInputStream extends FilterInputStream {
    // The pushback buffer
    protected byte[] buf;
    // The position within the pushback buffer from which the next byte will
    // be read
    protected int pos;

    public MyInputStream(InputStream in, int size) {
        super(in);
        if (size <= 0) {
            throw new IllegalArgumentException("size <= 0");
        }
        this.buf = new byte[size];
        this.pos = size;
    }

    public MyInputStream(InputStream in) {
        this(in, 1);
    }

    private void ensureOpen() throws IOException {
        if (in == null)
            throw new IOException("Stream closed");
    }

    @Override
    // This method returns the most recently pushed-back byte, if there is
    // one, and otherwise calls the <code>read</code> method of its underlying
    // input stream and returns whatever value that method returns
    public int read() throws IOException {
        ensureOpen();
        if (pos < buf.length) {
            return buf[pos++] & 0xff;
        }
        return super.read();
    }

    public int available() throws IOException {
        ensureOpen();
        int n = buf.length - pos;
        int avail = super.available();
        return n > (Integer.MAX_VALUE - avail)
                ? Integer.MAX_VALUE
                : n + avail;
    }

    public synchronized void close() throws IOException {
        if (in == null)
            return;
        in.close();
        in = null;
        buf = null;
    }

    public static void main(String[] args) {

    }
}
