package Task2_Serialization;

import java.io.Serializable;

/**
 * Class is a blueprint for creating droids
 */
class Droid implements Serializable {
    private String name;
    private int strength;
    private int life;
    private transient boolean isAlive;

    Droid(String name, int strength, int life, boolean isAlive) {
        this.name = name;
        this.strength = strength;
        this.life = life;
        this.isAlive = isAlive;
    }

    String getName() {
        return name;
    }

    int getStrength() {
        return strength;
    }

    int getLife() {
        return life;
    }

    boolean getIsAlive() {
        return isAlive;
    }
}
