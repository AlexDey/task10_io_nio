package Task2_Serialization;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 07 DEC 2018
 * Class initializes droidsShip, serializes and de-serializes it
 */
public class Ship {
    private static List<Droid> droidsShip = new ArrayList<>();

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        loadDroids();
        serializeDroids();
        ArrayList<Droid> deserializedShip = deserializeDroids();
        System.out.println(droidsShip.get(0).getName()
                .equals(deserializedShip.get(0).getName()));
        System.out.println(droidsShip.get(0).getIsAlive()
                == deserializedShip.get(0).getIsAlive());
    }

    private static void loadDroids() {
        droidsShip.add(new Droid("One", 50, 100, true));
        droidsShip.add(new Droid("One", 120, 80, true));
        droidsShip.add(new Droid("One", 75, 200, true));
    }

    private static void serializeDroids() throws IOException {
        FileOutputStream fos = new FileOutputStream("serializedDroids.dat");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(droidsShip);
        fos.close();
        oos.close();
    }

    private static ArrayList<Droid> deserializeDroids() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream("serializedDroids.dat");
        ObjectInputStream ois = new ObjectInputStream(fis);
        ArrayList<Droid> tempList = (ArrayList) ois.readObject();
        fis.close();
        ois.close();
        return tempList;
    }
}
