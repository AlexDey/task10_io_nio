package Task5_JavaCommentsReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 8 Dec 2018
 * Program reads a Java source-code file (the file name is provided
 * on the command line) and displays all the comments
 */
public class Application {
    private static List<String> list = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        readComments(args[0]);
        System.out.println("All file comments are below: ");
        list.forEach(System.out::println);
    }

    private static void readComments(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while (reader.ready()) {
            String line = reader.readLine().trim();
            if (line.startsWith("//")) {
                list.add(line);
            }
        }
    }
}
