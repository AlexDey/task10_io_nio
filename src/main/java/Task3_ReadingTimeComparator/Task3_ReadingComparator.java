package Task3_ReadingTimeComparator;

import java.io.*;
import java.util.Date;
// some comment
/**
 * @author Deyneka Oleksandr
 * @version 1.0 07/12/2018
 * This class compares reading and writing performance of usual and buffered
 * reader and compares performance of buffered reader with different buffer size
 */
public class Task3_ReadingComparator {
    private static FileInputStream fis;

    public static void main(String[] args) throws IOException {
        // file variable is created for calling on its length() method
        File file = new File("text.txt");
        System.out.println("File size is: " + file.length() / 1024 + " Kb");
        long bufferedTime = timeBufferedReader();
        System.out.println("Buffered reading lasted: " + bufferedTime + " ms");
        long usualTime = timeUsualReader();
        System.out.println("Usual reading lasted: " + usualTime + " ms");
        System.out.println(bufferedTime > usualTime ?
                "BufferedReader is faster!" : "Usual reader is faster!");
    }

    /**
     * This method is to be done...
     *
     * @return the execution time
     * @throws IOException exception
     */
    private static long timeUsualReader() throws IOException {
        fis = new FileInputStream("text.txt");
        // time start point
        Date dateStart = new Date();
        while (fis.available() > 0) {
            fis.read();
        }
        // time end point
        Date dateEnd = new Date();
        fis.close();
        return dateEnd.getTime() - dateStart.getTime();
    }

    /**
     * This method is to be done...
     *
     * @return the execution time
     * @throws IOException exception
     */
    private static long timeBufferedReader() throws IOException {
        fis = new FileInputStream("text.txt");
        BufferedInputStream bis = new BufferedInputStream(fis, 8 * 1024);
        Date dateStart = new Date();
        while (bis.available() > 0) {
            bis.read(new byte[1024]);
        }
        Date dateEnd = new Date();
        fis.close();
        bis.close();
        return dateEnd.getTime() - dateStart.getTime();
    }
}
