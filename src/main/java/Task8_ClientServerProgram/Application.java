package Task8_ClientServerProgram;

import java.io.IOException;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 10 Dec 2018
 * Task: write client-server program using NIO
 * E.g. you have one server and multiple clients.
 * A client can send direct messages to other client.
 */
public class Application {

    public static void main(String[] args) throws IOException {
        new NioClient().run();
        new NioServer().run();
    }
}
