package Task8_ClientServerProgram;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.spi.SelectorProvider;

import static Task8_ClientServerProgram.NioClient.ADDRESS;
import static Task8_ClientServerProgram.NioClient.PORT;

/**
 * To be done...
 */
public class NioServer {
    private Selector selector;
    private ByteBuffer readBuffer = ByteBuffer.allocate(1000);

    NioServer() throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);
        InetSocketAddress isa = new InetSocketAddress(ADDRESS, PORT);
        selector = SelectorProvider.provider().openSelector();
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

    }

    public void run() {

    }
}
