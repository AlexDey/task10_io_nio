package Task6_DirectoryContent;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Program displays the contents of a specific directory (file and
 * folder names + their attributes) with the possibility of setting the current
 * directory (similar to “dir” and “cd” command line commands).
 */
public class Application {
    private Map<String, String> menu = new LinkedHashMap<>();
    private Map<String, Printable> methods = new LinkedHashMap<>();
    private File directory;


    /**
     * entry point
     *
     * @param args main arguments
     */
    public static void main(String[] args) throws IOException {
        Application app = new Application();
        app.initMaps();
        while (true) {
            System.out.println("Please, make your choice: ");
            app.printMenu();
            app.readInput();
        }
    }

    /**
     * method initializes "menu" and "methods"
     */
    private void initMaps() {
        menu.put("1", " - Set directory");
        menu.put("2", " - Display current directory size");
        menu.put("3", " - Display current directory files and folders");
        menu.put("0", " - Exit");

        methods.put("1", this::setDirectory);
        methods.put("2", this::displayDirSize);
        methods.put("3", this::displayFiles);
        methods.put("0", this::startOption0);
    }

    /**
     * method prints out the main menu
     */
    private void printMenu() {
        menu.forEach((k, v) -> System.out.println(k + v));
    }

    /**
     * method deals with user's input
     */
    private void readInput() throws IOException {
        Scanner scanner = new Scanner(System.in);
        String res = scanner.nextLine();
        while (!methods.keySet().contains(res)) {
            System.out.println("You entered an invalid value. Please, try again...");
            res = scanner.nextLine();
        }
        methods.get(res).print();
    }

    private void setDirectory() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter the directory path: ");
        String path = scanner.nextLine();
        directory = new File(path);
        while (!directory.isDirectory()) {
            System.out.println("Directory with name " +
                    path + " doesn't exist. Try again");
            setDirectory();
        }
    }

    private void displayDirSize() {
        File[] content = directory.listFiles();
        long totalSize = 0;
        assert content != null;
        for (File f : content) {
            totalSize += f.length();
        }
        System.out.println("Directory size is : " +
                totalSize / 1024 + " Kb");
    }

    /**
     * method displays a list of a directory's files and subdirectories
     */
    private void displayFiles() {
        File[] content = directory.listFiles();
        System.out.println(directory.toPath().toString());
        assert content != null;
        int maxLength = 0;
        for (File f : content) {
            if (f.getName().length() > maxLength) {
                maxLength = f.getName().length();
            }
        }
        for (File f : content) {
            String name = appendSpacesToName(f.getName(), maxLength);
            if (f.isDirectory()) {
                System.out.println("   Directory " + name
                        + f.length() / 1024 + " Kb");
            } else {
                System.out.println("   File      " + name
                        + f.length() / 1024 + " Kb");
            }
        }
    }

    /**
     * method appends some spaces to the file names
     *
     * @param name      file name
     * @param maxLength the max length
     * @return string
     */
    private String appendSpacesToName(String name, int maxLength) {
        StringBuilder sb = new StringBuilder(name);
        while (sb.length() <= maxLength + 1) {
            sb.append(" ");
        }
        return sb.toString();
    }

    /**
     * method ends the program
     */
    private void startOption0() {
        System.exit(0);
    }
}
