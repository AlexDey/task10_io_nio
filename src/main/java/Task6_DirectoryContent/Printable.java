package Task6_DirectoryContent;

import java.io.IOException;

@FunctionalInterface
public interface Printable {
    void print() throws IOException;
}
